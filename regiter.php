<?php
require 'config/db.php';
require 'libraries/database.php';
require 'helper/validate.php';
require 'helper/user.php';
db_connect($db);
// users(id, username, email, password, first_name, last_name, phone, address, last_login)
//1: login = username or email and password
//2: dang ky user
global $error, $username, $email, $password, $first_name, $last_name, $phone, $address;
if ($_SERVER['REQUEST_METHOD'] == "POST") {
	$error = array();
	if (empty($_POST['username'])) {
		$error['username'] = "This is not empty	";
	} else {
		$username = $_POST['username'];
	}

	if (empty($_POST['email'])) {
		$error['email'] = "this is not empty";
	} else {
		if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			$error['email'] = "$email is not a valid email address";
		} else {
			$email = $_POST['email'];
		}
	}

	if (empty($_POST['password'])) {
		$error['password'] = "this is not empty";
	} else {
		$password = md5($_POST['password']);
	}

	if (empty($_POST['first_name'])) {
		$error['first_name'] = "This is not empty";
	} else {
		$first_name = $_POST['first_name'];
	}
	if (empty($_POST['last_name'])) {
		$error['last_name'] = "This is not empty";
	} else {
		$last_name = $_POST['last_name'];
	}
	if (empty($_POST['phone'])) {
		$error['phone'] = "this is not empty";
	} else {
		$phone = $_POST['phone'];
	}
	if (empty($_POST['address'])) {
		$error['address'] = "this is empty";
	} else {
		$address = $_POST['address'];
	}
	if (empty($error)) {
		$user = array(
			'username' => $username,
			'email' => $email,
			'password' => $password,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'phone' => $phone,
			'address' => $address,
			'last_login' => time(),
		);
		if (add_user($user)) {
			redirect('login.php');
		}
	}
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../../../favicon.ico">

	<title>Signin Template for Bootstrap</title>

	<!-- Bootstrap core CSS -->
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
		<h5 class="my-0 mr-md-auto font-weight-normal">Company name</h5>
		<nav class="my-2 my-md-0 mr-md-3">
			<a class="p-2 text-dark" href="#">Features</a>
			<a class="p-2 text-dark" href="#">Enterprise</a>
			<a class="p-2 text-dark" href="#">Support</a>
			<a class="p-2 text-dark" href="#">Pricing</a>
		</nav>
		<a class="btn btn-outline-primary" href="#">Sign up</a>
	</div>
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-9">
			<h1 class="col-md-7 text-center">register</h1>
			<form method="post" class="needs-validation" novalidate>
				<div class="form-group row">
					<label for="staticEmail" class="col-sm-2 col-form-label">Username</label>
					<div class="col-sm-6">
						<?php if (isset($error['username'])): ?>
							<input type="text" class="form-control is-invalid" name="username" id="validationCustom03" placeholder="username" required>
							<div class="invalid-feedback">
								<?php echo $error['username']; ?>
							</div>
						<?php else: ?>
							<input type="text" name="username" class="form-control" value="<?php set_value('username');?>" placeholder="username">
						<?php endif;?>
					</div>
				</div>
				<div class="form-group row">
					<label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
					<div class="col-sm-6">
						<?php if (isset($error['email'])): ?>
							<input type="text" class="form-control is-invalid" name="email" id="validationCustom03" placeholder="email" required>
							<div class="invalid-feedback">
								<?php echo $error['email']; ?>
							</div>
						<?php else: ?>
							<input type="text" name="email" class="form-control" value="<?php set_value('email');?>" placeholder="email">
						<?php endif;?>
					</div>
				</div>
				<div class="form-group row">
					<label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
					<div class="col-sm-6">
						<?php if (isset($error['password'])): ?>
							<input type="text" class="form-control is-invalid" name="password" id="validationCustom03" placeholder="password" required>
							<div class="invalid-feedback">
								<?php echo $error['password']; ?>
							</div>
						<?php else: ?>
							<input type="password" name="password" class="form-control" placeholder="password">
						<?php endif;?>
					</div>
				</div>
				<div class="form-group row">
					<label for="staticFirst_name" class="col-sm-2 col-form-label">First name</label>
					<div class="col-sm-6">
						<?php if (isset($error['first_name'])): ?>
							<input type="text" class="form-control is-invalid" name="first_name" id="validationCustom03" placeholder="first_name" required>
							<div class="invalid-feedback">
								<?php echo $error['first_name']; ?>
							</div>
						<?php else: ?>
							<input type="text" name="first_name" class="form-control" placeholder="first name">
						<?php endif;?>
					</div>
				</div>
				<div class="form-group row">
					<label for="staticEmail" class="col-sm-2 col-form-label">Last name</label>
					<div class="col-sm-6">
						<?php if (isset($error['last_name'])): ?>
							<input type="text" class="form-control is-invalid" name="last_name" id="validationCustom03" placeholder="last_name" required>
							<div class="invalid-feedback">
								<?php echo $error['last_name']; ?>
							</div>
						<?php else: ?>
							<input type="text" name="last_name" class="form-control" placeholder="last name">
						<?php endif;?>
					</div>
				</div>
				<div class="form-group row">
					<label for="staticEmail" class="col-sm-2 col-form-label">Phone</label>
					<div class="col-sm-6">
						<?php if (isset($error['phone'])): ?>
							<input type="text" class="form-control is-invalid" name="phone" id="validationCustom03" placeholder="phone" required>
							<div class="invalid-feedback">
								<?php echo $error['phone']; ?>
							</div>
						<?php else: ?>
							<input type="text" name="phone" class="form-control" placeholder="phone">
						<?php endif;?>
					</div>
				</div>
				<div class="form-group row">
					<label for="staticEmail" class="col-sm-2 col-form-label">Address</label>
					<div class="col-sm-6">
						<?php if (isset($error['address'])): ?>
							<input type="text" class="form-control is-invalid" name="address" id="validationCustom03" placeholder="address" required>
							<div class="invalid-feedback">
								<?php echo $error['address']; ?>
							</div>
						<?php else: ?>
							<input type="text" name="address" class="form-control" placeholder="address">
						<?php endif;?>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-8 text-center">
						<button type="submit" class="btn btn-primary">Sign up</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>